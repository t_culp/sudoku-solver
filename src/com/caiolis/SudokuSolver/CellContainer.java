package com.caiolis.SudokuSolver;

public class CellContainer implements Solvable {
	
	private Cell[] cells = new Cell[9];
	private int numOfCells = 0;
	private boolean solved = false;
	
	/*
	 * Returns whether a cell within the container has changed: an impossibility has been added to the cell
	 */
	public boolean check() {
		boolean checkAgain = true;
		boolean changed = false;
		while (checkAgain) {
			checkAgain = false;
			for (Cell cell : cells)
				if (!cell.isSolved()) {
					if (cell.check(cells))
						changed = true;
					if (cell.isSolved())
						checkAgain = true;
				}	
		}
		/* 
		 * here, checkAgain is being recycled and used to see if the entire 
		 * container is solved 
		 */
		checkAgain = true;
		for (Cell cell: cells)
			if (!cell.isSolved())
				checkAgain = false;
		if (checkAgain)
			setSolved();
		return changed;
	}
	
	public void add(Cell cell) {
		if (numOfCells < 9) {
			cells[numOfCells++] = cell;
		}
	}

	public void setSolved(int... number) {
		solved = true;
	}

	public boolean isSolved() {
		return solved;
	}
	
} // close CellContainer
