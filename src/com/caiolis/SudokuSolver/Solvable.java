package com.caiolis.SudokuSolver;

public interface Solvable {
	
	/*
	 * Used on an object to signify that it is (completely) solved
	 */
	public void setSolved(int... number);
	
	/*
	 * Used to check if an object is (completely) solved
	 */
	public boolean isSolved();
	
} // close Solvable
