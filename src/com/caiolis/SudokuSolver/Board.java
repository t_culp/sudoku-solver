package com.caiolis.SudokuSolver;

public class Board {
	
	private Cell[] cells = new Cell[81];
	private CellContainer[] rows = new CellContainer[9];
	private CellContainer[] columns = new CellContainer[9];
	private CellContainer[] blocks = new CellContainer[9];
	
	private static final int[] INITIALIZE = {0, 0, 0, 0, 0, 7, 0, 1, 0, 
		4, 0, 0, 6, 0, 0, 0, 0, 0, 
		0, 0, 7, 2, 1, 0, 3, 6, 0, 
		0, 1, 0, 0, 7, 0, 0, 3, 8,
		0, 5, 0, 0, 0, 0, 0, 7, 0,
		2, 7, 0, 0, 6, 0, 0, 5, 0, 
		0, 3, 8, 0, 4, 9, 1, 0, 0, 
		0, 0, 0, 0, 0, 6, 0, 0, 3, 
		0, 2, 0, 7, 0, 0, 0, 0, 0
	};
	private static final int LIMIT = 10000;
	
	public static void main(String[] args) {
		Board board = new Board(INITIALIZE);
		int[] solution = board.solve();
		for (int i = 0; i < 9; ++i) {
			for (int j = 0; j < 9; ++j) {
				System.out.print(solution[(i * 9) + j] + "\t");
			}
			System.out.print("\n");
		}
	}
	
	public Board(int[] rawNumbers) {
		if (rawNumbers.length != 81) {
			System.exit(1);
		}
		for (int i = 0; i < 9; ++i) {
			rows[i] = new CellContainer();
			columns[i] = new CellContainer();
			blocks[i] = new CellContainer();
		}
		for (int i = 0; i < 81; ++i)
			cells[i] = new Cell(rawNumbers[i]);
		for (int i = 0; i < 81; ++i) {
			int quotient = i / 9; // row number
			int remainder = i % 9; // column number
			rows[quotient].add(cells[i]);
			columns[remainder].add(cells[i]);
			quotient /= 3;
			remainder /= 3;
			// The number inside the brackets gives the block number... check the math.
			blocks[(quotient * 3) + remainder].add(cells[i]);
		}	
	} // close constructor(int[])
	
	/*
	 * This constructor is used to create copies of the current board when the program reaches a dead end
	 * with the method of deduction.
	 */
	public Board(Board board) {
		for (int i = 0; i < 81; ++i)
			cells[i] = new Cell(board.cells[i].getNumber());
		for (int i = 0; i < 9; ++i) {
			rows[i] = new CellContainer();
			columns[i] = new CellContainer();
			blocks[i] = new CellContainer();
		}
		for (int i = 0; i < 9; ++i)
			for (int j = 0; j < 9; ++j) {
				int position = (i * 9) + j;
				rows[i].add(cells[position]);
				columns[j].add(cells[position]);
				blocks[((i / 3) * 3) + (j / 3)].add(cells[position]);
			}
	} // close constructor(Board)
	
	public int[] solve() {
		boolean solved = false;
		boolean changed = true;
		int crazinessChecker = 0;
		/*
		 * The loop is broken when:
		 * 1) The board is solved, or
		 * 2) there are no more changes to the cells, or
		 * 3) the crazinessChecker >= 10000
		 */
		while (!solved && changed && crazinessChecker++ < LIMIT) {
			solved = true;
			changed = false;
			for (CellContainer row : rows) {
				if (row.isSolved())
					continue;
				if (row.check())
					changed = true;
				if (!row.isSolved())
					solved = false;
			}
			for (CellContainer column : columns) {
				if (column.isSolved())
					continue;
				if (column.check())
					changed = true;
				if (!column.isSolved())
					solved = false;
			}
			for (CellContainer block : blocks) {
				if (block.isSolved())
					continue;
				if (block.check())
					changed = true;
				if (!block.isSolved())
					solved = false;
			}
		}
		if (crazinessChecker == LIMIT)
			System.out.println("Exceeded " + LIMIT + " loops in the solve function.");
		else if (!changed)
			System.out.println("No more changes have been made.\nLoops in the solve function: " + crazinessChecker);
		int[] solution = new int[81];
		for (int i = 0; i < 81; ++i)
			solution[i] = cells[i].getNumber();
		return solution;
	} // close solve
	
} // close Board
