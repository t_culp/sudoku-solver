package com.caiolis.SudokuSolver;

import java.util.ArrayList;

public class Cell implements Solvable {
	
	private int[] impossibilities = new int[8];
	private int numOfImpossibilities = 0;
	private int number;
	private boolean solved = false;
	private static final ArrayList<Integer> oneThroughNine = new ArrayList<Integer>();
	
	static {
		for (int i = 1; i < 10; ++i)
			oneThroughNine.add(i);
	}
	
	public Cell(int number) {
		if (number != 0)
			setSolved(number);
	} // close constructor
	
	/*
	 * Returns whether or not the cell has changed: if an impossibility has been added.
	 */
	public boolean check(Cell[] cells) {
		boolean changed = false;
		for (Cell cell : cells) {
			/* 
			 * note that it isn't needed to check to see if the cell in this loop is *this* cell
			 * as it should be checked prior to calling this method whether or not this cell *isSolved*.
			 */
			if (cell.isSolved()) {
				int possibleImpossibility = cell.getNumber();
				boolean match = false;
				int i = -1;
				while (!match && ++i < 8)
					if (possibleImpossibility == impossibilities[i])
						match = true;
				if (!match) {
					impossibilities[numOfImpossibilities++] = possibleImpossibility;
					changed = true;
				}
			}
		}
		if (numOfImpossibilities == 8) {
			ArrayList<Integer> copyOfList = new ArrayList<Integer>(oneThroughNine);
			for (int impossibility : impossibilities)
				copyOfList.remove((Integer)impossibility);
			setSolved(copyOfList.get(0));
		}
		return changed;
	} // close check
	
	public int getNumber() {
		return number;
	} // close getNumber

	public void setSolved(int... number) {
		this.number = number[0];
		solved = true;
		numOfImpossibilities = 0;
	} // close setSolved

	public boolean isSolved() {
		return solved;
	} // close isSolved
	
} // close Cell
